/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Game event queue.
 */

#pragma once

#include "../common/types.h"

//==================== MODULE STRUCTURES
/** Game event structure */
typedef struct {
        u32 game_id;    //!< Game ID
        u32 ev_no;    //!< Event no
        u8 ev_type;     //!< Event type
        u8 data0;       //!< Data field 0
        u32 data1;      //!< Data field 1
        u32 data2;      //!< Data field 2
        u8 *extra;      //!< Extra data
        u16 extra_l;    //!< Size of extra data
} event_t;

typedef struct events_t events_t;

//==================== INTERFACE
/** Creates new event queue. */
events_t *ev_create();

/** Destroys event queue. */
void ev_destroy(events_t *events);

/** Unpack event from data buffer. */
event_t *ev_unpack(u8 *data, u16 data_l, u16 *len);

/** Get already used events. */
u32 ev_sent(events_t *events);

/** Get queue length. */
u32 ev_len(events_t *events);

/** Append event to queue.*/
void ev_append(events_t *events, event_t *ev);

/** Returns next event to send.
 * NULL pointer is returned if there's no new events to send. */
event_t *ev_get_next(events_t *events);