/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client entry point.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>


#include "resources.h"
#include "config.h"
#include "routines.h"
#include "logs.h"
#include "../common/colors.h"
#include "../common/utils.h"

//============= Module vars
/** Pointer for SIGINT handler to signalize client kill. */
static volatile u8 *over;

//============= Module functions
/** SIGINT handler */
static void sig_int(int sig);

/** Loads and validates client configuration from command line args. */
static void load_config(int argc, char **argv, cconf_t *conf);

/** Print user friendly instructions, how use this app. */
static void print_usage();

//============= Implementation...
/** Entry point.. */
int main(int argc, char **argv)
{
        cres_t *res; // Client resources
        cconf_t conf;
        load_config(argc, argv, &conf);

        res = client_create(&conf);
        log_init(client_clean, res);
        over = &(res->over);
        /* Let us to softly kill client with Ctrl+C */
        if (signal(SIGINT, sig_int) == SIG_ERR)
                fatal_sys("Trying to set SIGINT handler.");
        client_run(res);
        client_clean(res);
        return EXIT_SUCCESS;
}

void validate_player_name(char *name, cconf_t *conf)
{
        if (strlen(name) > PLAYER_NAME_SIZE)
                fatal(MSG_TYPE_PARAMS, "Player name is to long! Max length is %d.", PLAYER_NAME_SIZE);
        for (int i = 0; i < strlen(name); ++i) {
                if (name[i] < PLAYER_CHAR_MIN || PLAYER_CHAR_MAX < name[i])
                        fatal(MSG_TYPE_PARAMS, "Player name contains invalid characters!");
        }
        strncpy(conf->player_name, name, PLAYER_NAME_SIZE);
        conf->player_name[strlen(name)] = 0;
}

static void validate_host(char *host, u8 *addr_type, char *remote_host, in_port_t *p)
{
        char pbuff[6];
        uint64_t port;
        *addr_type = get_host_data(host, remote_host, pbuff);
        if (*pbuff) {
                port = validate_uint(pbuff);
                if (port > PORT_NUM_MAX)
                        fatal(MSG_TYPE_PARAMS, "Port number %d is too large! Max: %d", port, PORT_NUM_MAX);
                *p = (in_port_t) port;
        }
}

static void load_config(int argc, char **argv, cconf_t *conf)
{
        strcpy(conf->ghost, DEFAULT_GUI_HOST);
        conf->gport = DEFAULT_GUI_PORT;
        conf->sport = DEFAULT_SERVER_PORT;
        conf->gaddr_type = ADDR_TYPE_IP4;
        conf->saddr_type = ADDR_TYPE_IP4;

        if (argc <= 2) {
                print_usage();
                fatal(MSG_TYPE_PARAMS, "Specify player and game host at least...");
        }
        if (argc > 2) {
                validate_player_name(argv[1], conf);
                validate_host(argv[2], &conf->saddr_type, conf->shost, &conf->sport);
        }
        if (argc > 3)
                validate_host(argv[3], &conf->gaddr_type, conf->ghost, &conf->gport);
}

static void print_usage()
{
        fprintf(stderr, "\n\t./siktacka-client player_name game_host[:port] [gui_host[:port]]\n\n");
        fprintf(stderr, FYELLOW "\tplayer_name: " NONE "\tyour name, pass empty string if you want "
                "to be observer only\n");
        fprintf(stderr, FYELLOW "\tgame_host:" NONE "\tgame server host name (or IPv4/6 address)."
                "\t\tDefault port: %d\n", DEFAULT_SERVER_PORT);
        fprintf(stderr, FYELLOW "\tgui_host:" NONE "\tGUI server host name (or IPv4/6 address)."
                "\t\tDefault host: %s\t\tDefault port: %d\n\n", DEFAULT_GUI_HOST, DEFAULT_GUI_PORT);
}

static void sig_int(int sig)
{
        info(MSG_TYPE_QUIT, "Shutdown client...");
        *over = TRUE;
}