/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Gamestate module.
 */

#include <malloc.h>
#include "game.h"
#include "logs.h"
#include "gui_cmd.h"
#include "config.h"

//==================== MODULE STRUCTURES
/** Game state structure. */
typedef struct game_t {
        u32 game_id;    //!< Game session id
        i8 turn_dir;    //!< Turning direction
        events_t *events;       //!< Game event queue
} game_t;

//==================== HELPER METHODS
/** Analyze event content and do action. */
static void handle_event(game_t *game, event_t *event)
{
        if (event->ev_type == EVT_NEWGAME) {
                /* Whenever NEW GAME event is received, begin new game.
                 * Even if GAME OVER event was not received.
                 * ... GUI ignores GAME OVER...so doesn't matter. */
                game->game_id = event->game_id;
                ev_destroy(game->events);
                game->events = ev_create();
                ev_append(game->events, event);
                info("NEW GAME", "game id: %u", event->game_id);
                info("NEW GAME", "evno: %u, ev_len: %u", event->ev_no, ev_len(game->events));
        } else if (event->game_id == game->game_id &&
                   event->ev_no == ev_len(game->events)) {
                info("OTHER EV", "evno: %u, ev_len: %u", event->ev_no, ev_len(game->events));
                /* If it's not NEW GAME append only events from the same game.
                 * Events sequence is also validated. */
                ev_append(game->events, event);
        }
}

//==================== INTERFACE
game_t *game_create()
{
        game_t *game = calloc(1, sizeof(game_t));
        return game;
}

void game_clean(game_t **game)
{
        if (!*game)
                return;
        ev_destroy((*game)->events);
        (*game)->events = NULL;
        free(*game);
        *game = NULL;
}

i8 *game_dir_ptr(game_t *game)
{
        return &game->turn_dir;
}

void game_cmd_handler(i8 *dir, i8 cmd)
{
        switch (*dir) {
                case TURN_DIR_F:
                        if (cmd == CMD_R_DW_N) {
                                *dir = TURN_DIR_R;
                                info("DIR", "RIGHT");
                        } else if (cmd == CMD_L_DW_N) {
                                *dir = TURN_DIR_L;
                                info("DIR", "LEFT");
                        }
                        break;

                case TURN_DIR_R:
                        if (cmd == CMD_R_UP_N) {
                                *dir = TURN_DIR_F;
                                info("DIR", "FORWARD");
                        }
                        break;

                case TURN_DIR_L:
                        if (cmd == CMD_L_UP_N) {
                                *dir = TURN_DIR_F;
                                info("DIR", "FORWARD");
                        }
                        break;
        }
}

void game_parse_events(game_t *game, u8 *buff, u16 len)
{
        u32 game_id;
        /* First, get game ID. */
        game_id = ntohl(*((u32 *) buff));
        info("GAME EVENTS", "Game ID: %u", game_id);
        len -= 4;
        buff += 4;
        event_t *ev;
        u16 ev_l;
        /* Now, read all events in datagram. */
        while (len > 0) {
                if ((ev = ev_unpack(buff, len, &ev_l)) == NULL) {
                        warning("GAME EVENTS", "Event invalid!");
                        break; // Data invalid. Finish now.
                }
                ev->game_id = game_id;
                handle_event(game, ev);
                len -= ev_l;
                buff += ev_l;
        }
}

u32 game_req_event(game_t *game)
{
        return ev_len(game->events);
}

u32 game_last_sent(game_t *game)
{
        return ev_sent(game->events);
}

event_t *game_get_next_event(game_t *game)
{
        return ev_get_next(game->events);
}