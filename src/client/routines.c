/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client main routines.
 */

#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <errno.h>
#include "routines.h"
#include "../common/utils.h"
#include "logs.h"
#include "resources.h"

//============= Module helper functions

/** Sends data to game server and gui server,
* if any available. */
static void client_send(cres_t *res)
{
        /* If message flag is set, send msg to game server.
         * Flag is set by clock with given frequency. */
        if (res->new_msg) {
                warning("NEX", "next: %u", game_req_event(res->game));
                ghost_send(res->ghost, *game_dir_ptr(res->game), game_req_event(res->game));
                res->new_msg = FALSE;
        }
        /* If any game event msg left in the buffer, send it to GUI server.*/
        while (game_last_sent(res->game) != game_req_event(res->game)) {
                gui_send_msg(res->gui, game_get_next_event(res->game));
        }
}

/** Receives data from game server and gui server.
* If there's no data to send in main loop, goes sleep. */
static void client_recv(cres_t *res)
{
        struct timeval timeout;
        int retval;

        timeout.tv_sec = 0;
        timeout.tv_usec = 0;

        FD_ZERO(&res->rd_sock_set);
        FD_SET(gui_sock(res->gui), &res->rd_sock_set);
        FD_SET(ghost_sock(res->ghost), &res->rd_sock_set);

        /* If client should do it's cyclic routines or send queued events
         * to GUI server, then cannot go sleep and select() checks
         * if any data is available and return immediately.
         * Otherwise, there's nothing to do and goes sleep exact
         * amount of time to meet timming requirements. */
        if (!res->new_msg && (game_last_sent(res->game) == game_req_event(res->game))) {
                timeout.tv_usec = clk_left(res->clk) / 1000;
        }
        retval = select(res->max_sock + 1, &res->rd_sock_set, 0, 0, &timeout);

        if (retval < 0) {
                /* Check whether signal caused return from select() */
                if (errno != EINTR)
                        fatal_sys("Fail on select.");
                else {
                        client_clean(res);
                        exit(EXIT_SUCCESS);
                }
        } else if (retval > 0) {
                if (FD_ISSET(gui_sock(res->gui), &res->rd_sock_set)) {
                        /* Incoming data on TCP socket from GUI.
                         * Receive and parse data. GUI module calls cmd handler on each
                         * recognized command. */
                        gui_recv(res->gui);
                }

                if (FD_ISSET(ghost_sock(res->ghost), &res->rd_sock_set)) {
                        /* Incoming data on UDP socket from game server. */
                        u16 len;
                        u8 * data = ghost_recv(res->ghost, &len);
                        game_parse_events(res->game, data, len);
                }
        }
}

//==================== INTERFACE
cres_t *client_create(cconf_t *conf)
{
        info(MSG_TYPE_INIT, "Client init...");

        cres_t *res = calloc(1, sizeof(cres_t));
        res->over = FALSE;
        res->new_msg = FALSE;

        /* Create clock */
        res->clk = clk_create(NANO_IN_SEC / CLIENT_REFRESH_RATE, &res->new_msg);
        /* Create game */
        u64 ss_id = get_epoch_usec();
        res->game = game_create();
        /* Establish connections with servers. */
        res->gui = gui_connect(conf->ghost, conf->gport, conf->gaddr_type, game_dir_ptr(res->game), game_cmd_handler);
        res->ghost = ghost_connect(conf->shost, conf->sport, conf->saddr_type, conf->player_name, ss_id);
        int gsock = gui_sock(res->gui);
        res->max_sock = ghost_sock(res->ghost);
        res->max_sock = (res->max_sock < gsock ? gsock : res->max_sock);
        info(MSG_TYPE_INIT, "Client init done!");
        return res;
}

void client_clean(cres_t *res)
{
        info("EXIT", "Goodbye!");
        game_clean(&res->game);
        gui_close(&res->gui);
        ghost_close(&res->ghost);
        free(res);
}

void client_run(cres_t *res)
{
        while (!res->over) {
                client_send(res);
                clk_update(res->clk);
                client_recv(res);
        }
}
