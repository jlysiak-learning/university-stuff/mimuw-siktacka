/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client main routines.
 */

#pragma once

#include "config.h"
#include "resources.h"

/** Initializes clients structs and connects
 * with game and server hosts. */
cres_t * client_create(cconf_t *conf);

/** Cleanup before exit... */
void client_clean(cres_t *res);

/** Does all client's stuff... */
void client_run(cres_t *res);
