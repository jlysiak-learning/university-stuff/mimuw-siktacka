/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * GUI server communication.
 */

// Using -std=c11, gcc can't recoginze struct addrinfo... :(
#define _DEFAULT_SOURCE

#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <unistd.h>
#include "gui.h"
#include "../common/defs.h"
#include "logs.h"
#include "gui_cmd.h"

//==================== MODULE DEFS
#define GUI_BUFF_SIZE           2048u

//==================== MODULE STRUCTURES
typedef struct gui_t {
        /* Network */
        char host[HOST_NAME_SIZE];
        u16 port;
        u8 addr_type;
        saddr_in addr;  //!< GUI server address structure
        saddr_in6 addr6;  //!< GUI server address structure
        saddr_l addr_l; //!< GUI server address structure length
        int sock;      //!< Gui server socket file descriptor

        /* Communication */
        char tx_buff[GUI_BUFF_SIZE]; //!< Transmission buffer
        u16 tx_len;     //!< Transmission buffer length
        char rx_buff[GUI_BUFF_SIZE]; //!< Reception buffer
        u16 rx_len;     //!< Reception buffer length

        /* Commands */
        i8 *cmd_h_arg;    //!< Command handler argument
        void (*cmd_handler)(i8 *, i8); //!< Command handler

        /* Players list */
        char players[MAX_CLIENTS][PLAYER_NAME_SIZE + 1];
} gui_t;

//==================== HELPERS

/** Compare received message with commands strings. */
static i8 check_command(char *cmd_str)
{
        if (!strcmp(cmd_str, CMD_L_UP))
                return CMD_L_UP_N;
        if (!strcmp(cmd_str, CMD_L_DW))
                return CMD_L_DW_N;
        if (!strcmp(cmd_str, CMD_R_UP))
                return CMD_R_UP_N;
        if (!strcmp(cmd_str, CMD_R_DW))
                return CMD_R_DW_N;
        return CMD_UNKNOW;
}

/** Parses received data over TCP from GUI server.
 * Correct commands passes to control function.
 * Any incorrect data ignores. */
static void parse_commands(gui_t *gui)
{
        size_t i = 0;
        int8_t cmd;
        while (i < gui->rx_len) {
                if (i == GUI_BUFF_SIZE - 1) {
                        /* We received big amount of data so far and still
                         * there's no line end...
                         * Ignore all buffer. */
                        warning("GUI", "Junk data from GUI server...");
                        gui->rx_len = 0;
                        break;
                }
                if (gui->rx_buff[i] != '\n') {
                        i++;
                        continue;
                }
                gui->rx_buff[i] = 0;
                cmd = check_command(gui->rx_buff);
                (*(gui->cmd_handler))(gui->cmd_h_arg, cmd); // Call this ugly handler... :)
                memmove(gui->rx_buff, gui->rx_buff + i + 1, i + 1);
                gui->rx_len -= i + 1;
                i = 0;
        }
}

static void prepare_new_game_msg(gui_t *gui, char *buff, u32 w, u32 h, char *players, u16 names_l)
{
        u8 pl_cnt = 0;
        u8 pl_len;
        sprintf(buff, "%s %u %u ", "NEW_GAME", w, h);
        while (names_l) {
                strcpy(gui->players[pl_cnt++], players);
                pl_len = strlen(players);
                names_l -= pl_len + 1;
                players += pl_len + 1;
        }

        for (u8 i = 0; i < pl_cnt; ++i) {
                strcat(buff, gui->players[i]);
                strcat(buff, " ");
        }
        buff[strlen(buff) - 1] = '\n';
}

static void prepare_pixel_msg(char *buff, char *player, u32 x, u32 y)
{
        sprintf(buff, "%s %u %u %s\n", "PIXEL", x, y, player);
}

static void prepare_pelim_msg(char *buff, char *player)
{
        sprintf(buff, "%s %s\n", "PLAYER_ELIMINATED", player);
}

//==================== INTERFACE
gui_t *gui_connect(const char *host, u16 port, u8 type, i8 *hnd_arg, void (*cmd_handler)(i8 *, i8))
{
        gui_t *gui = calloc(1, sizeof(gui_t));
        strcpy(gui->host, host);
        gui->port = port;
        gui->addr_type = type;
        gui->cmd_handler = cmd_handler;
        gui->cmd_h_arg = hnd_arg;

        /* Convert host name */
        struct addrinfo addr_hints;
        struct addrinfo *addr_result;
        memset(&addr_hints, 0, sizeof(struct addrinfo));
        addr_hints.ai_family = (type == ADDR_TYPE_IP6 ? AF_INET6 : AF_INET);
        addr_hints.ai_socktype = SOCK_STREAM;
        addr_hints.ai_protocol = IPPROTO_TCP;
        addr_hints.ai_flags = 0;
        addr_hints.ai_addrlen = 0;
        addr_hints.ai_addr = NULL;
        addr_hints.ai_canonname = NULL;
        addr_hints.ai_next = NULL;
        int retval;
        if ((retval = getaddrinfo(host, NULL, &addr_hints, &addr_result)) != 0)
                fatal("ADDR_INFO", "Convert host/port with getaddrinfo! (msg: %s)", gai_strerror(retval));
        freeaddrinfo(addr_result);

        if (type == ADDR_TYPE_IP6) {
                gui->sock = socket(AF_INET6, SOCK_STREAM, 0);
                gui->addr6.sin6_family = AF_INET6;
                gui->addr6.sin6_port = htons(port);
                gui->addr6.sin6_addr = ((saddr_in6 *) (addr_result->ai_addr))->sin6_addr;
                gui->addr_l = sizeof(gui->addr6);
        } else {
                gui->sock = socket(AF_INET, SOCK_STREAM, 0);
                gui->addr.sin_family = AF_INET;
                gui->addr.sin_port = htons(port);
                gui->addr.sin_addr.s_addr = ((saddr_in *) (addr_result->ai_addr))->sin_addr.s_addr;
                gui->addr_l = sizeof(gui->addr);
        }

        if (gui->sock < 0)
                fatal_sys("Fail during socket creation.");
        /* Off Nagle's algorithm during establishing TCP connection. */
        int opt = 1;
        if (setsockopt(gui->sock, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(int)) < 0)
                fatal_sys("Turning off Nagle's algorithm fail...");


        if (connect(gui->sock, (type == ADDR_TYPE_IP6 ? (saddr *) &gui->addr6 : (saddr *) &gui->addr), gui->addr_l) < 0)
                fatal_sys("Connect with GUI server failed!");
        info(MSG_TYPE_INIT, "Connection with GUI server established!");
        return gui;
}

void gui_close(gui_t **gui)
{
        close((*gui)->sock);
        free(*gui);
        *gui = NULL;
}

int gui_sock(gui_t *gui)
{
        return gui->sock;
}

void gui_recv(gui_t *gui)
{
        ssize_t len;
        len = recv(gui->sock, gui->rx_buff + gui->rx_len, GUI_BUFF_SIZE - gui->rx_len, 0);
        if (len < 0)
                fatal_sys("TCP recv() error");
        gui->rx_len += len;
        parse_commands(gui);
}

void gui_send_msg(gui_t *gui, event_t *ev)
{
        if (ev == NULL)
                return;

        ssize_t retval;
        char *buff = gui->tx_buff;
        switch (ev->ev_type) {
                case EVT_NEWGAME:
                        prepare_new_game_msg(gui, buff, ev->data1, ev->data2, (char *) ev->extra, ev->extra_l);
                        break;

                case EVT_PIXEL:
                        prepare_pixel_msg(buff, gui->players[ev->data0], ev->data1, ev->data2);
                        break;

                case EVT_PLELIM:
                        prepare_pelim_msg(buff, gui->players[ev->data0]);
                        break;
        }
        gui->tx_len = strlen(buff);
        /// @TODO check blocking TCP write...
        retval = send(gui->sock, (void *) buff, gui->tx_len, 0);
        if (retval < 0)
                fatal_sys("TCP segment sending error!");

        info(MSG_TYPE_SEND, "Segment sent [%u bytes]: %s", gui->tx_len, gui->tx_buff);
}