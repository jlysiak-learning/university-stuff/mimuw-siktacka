/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Game event queue.
 */

#include <zlib.h>
#include <malloc.h>
#include <string.h>
#include "event.h"
#include "resources.h"
#include "logs.h"
#include "../common/defs.h"
#include "../common/utils.h"

//==================== MODULE DEFS
#define INIT_SIZE       1024u

//==================== MODULE STRUCTURES
/** Event queue */
typedef struct events_t {
        event_t **events;
        u32 size;
        u32 len;
        u32 sent;
} events_t;

//==================== HELPERS

static void check_size(events_t *events)
{
        if (events->len < events->size)
                return;
        u32 newsize = 2 * events->size;
        event_t **newtab = calloc(newsize, sizeof(event_t *));
        memcpy(newtab, events->events, sizeof(event_t *) * events->size);
        free(events->events);
        events->events = newtab;
        events->size = newsize;
}

//==================== INTERFACE
events_t *ev_create()
{
        events_t *events = calloc(1, sizeof(events_t));
        events->events = calloc(INIT_SIZE, sizeof(event_t *));
        events->size = INIT_SIZE;
        return events;
}

void ev_destroy(events_t *events)
{
        if (!events)
                return;
        for (u32 i = 0; i < events->len; ++i) {
                if (events->events[i]) {
                        if (events->events[i]->extra)
                                free(events->events[i]->extra);
                        free(events->events[i]);
                }
        }
        free(events->events);
        free(events);
}

event_t *ev_unpack(u8 *data, u16 data_l, u16 *len)
{
        u32 crc_calc, crc_recv;
        u32 ev_len;
        *len = 0;

        /* Total length of events fields. */
        ev_len = ntohl(*((u32 *) data));
        /* Make dump */
        dump_data("EVENT", data, ev_len + 8);
        /* Get CRC sum first. From ev_len field to crc field. */
        crc_calc = (u32) crc32(0, data, ev_len + 4);
        /* Get received CRC. */
        crc_recv = ntohl(*((u32 *) (data + ev_len + 4)));
        if (crc_calc != crc_recv)
                return NULL; // CRC sums doesn't match. Ignore!

        event_t *ev = calloc(1, sizeof(event_t));
        ev->extra = NULL;
        data += 4; // Skip event len
        /* Event number */
        ev->ev_no = ntohl(*((u32 *) data));
        data += 4; // Skip event no
        /* Event type. */
        ev->ev_type = *data;
        data++;
        switch (ev->ev_type) {
                case EVT_NEWGAME:
                        /* Arena width. */
                        ev->data1 = ntohl(*((u32 *) data));
                        data += 4;
                        /* Arena height. */
                        ev->data2 = ntohl(*((u32 *) data));
                        data += 4;
                        /* Players */
                        ev->extra_l = ev_len - 13;
                        ev->extra = calloc(ev->extra_l, sizeof(u8));
                        memcpy(ev->extra, data, ev->extra_l);
                        break;

                case EVT_PIXEL:
                        /* Player id */
                        ev->data0 = *data;
                        data++;
                        /* Head x position. */
                        ev->data1 = ntohl(*((u32 *) data));
                        data += 4;
                        /* Arena height. */
                        ev->data2 = ntohl(*((u32 *) data));
                        data += 4;
                        break;

                case EVT_PLELIM:
                        /* Player id */
                        ev->data0 = *data;
                        data++;
                        break;

                case EVT_GAMEOVER:
                        /* No data... */
                        break;
        }
        *len = ev_len + 8; // event_* fields + crc (4B) + len (4B)
        return ev;
}

u32 ev_sent(events_t *events)
{
        if (!events)
                return 0;
        return events->sent;
}

u32 ev_len(events_t *events)
{
        if (!events)
                return 0;
        return events->len;
}

void ev_append(events_t *events, event_t *ev)
{
        if (!events)
                return;
        check_size(events);
        events->events[events->len++] = ev;
}

event_t *ev_get_next(events_t *events)
{
        if (!events)
                return NULL;
        if (events->sent == events->len)
                return NULL;
        return events->events[events->sent++];
}