/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Failure module.
 * Automatic cleaning on server fail.
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "logs.h"
#include "../common/colors.h"
#include "../common/defs.h"

//====== MODULE VARIABLES
/* This solution maybe is not beauty, but
 * there's no need to pass pointers everywhere. */
static u8 all_ready = FALSE;

/** Pointer to server resoruces. */
static cres_t *client_resources = NULL;

/** Pointer to server cleaning function. */
static void (*client_clean_f)(cres_t *) = NULL;

//====== INTERNAL HELPER METHODS
/** Print on output time in milis since start. */
static void print_uptime(FILE *out)
{
        u64 up = 0;
        if (all_ready && client_resources->clk != NULL)
                up = clk_uptime(client_resources->clk);
        fprintf(out, "[%15.3f]", up * 1000. / NANO_IN_SEC);
}

//====== MODULE INTERFACE
void log_init(void (* clean_f)(cres_t *), cres_t *resources)
{
        client_resources = resources;
        client_clean_f = clean_f;
        all_ready = TRUE;
}

void fatal_sys(const char *msg_fmt, ...)
{
        va_list fmt_args;
        print_uptime(stderr);
        fprintf(stderr, "[" FRED "SYSERR" NONE "]: " NONE);
        va_start(fmt_args, msg_fmt);
        vfprintf(stderr, msg_fmt, fmt_args);
        va_end(fmt_args);
        fprintf(stderr,"\n");
        print_uptime(stderr);
        fprintf(stderr, "[" FRED "ERRMSG" NONE "][" FYELLOW "%d" NONE "]: %s\n", errno, strerror(errno));

        if (all_ready)
                (*client_clean_f)(client_resources);
        exit(EXIT_FAILURE);
}

void fatal(const char *type, const char *msg_fmt, ...)
{
        va_list fmt_args;
        print_uptime(stderr);
        fprintf(stderr, "[" FRED "ERROR" NONE "][" FYELLOW "%s" NONE "]: ", type);
        va_start(fmt_args, msg_fmt);
        vfprintf(stderr, msg_fmt, fmt_args);
        va_end(fmt_args);
        fprintf(stderr, "\n");

        if (all_ready)
                (*client_clean_f)(client_resources);
        exit(EXIT_FAILURE);
}

void warning(const char *type, const char *msg_fmt, ...)
{
#ifdef __WARNING_ENABLED__
        va_list fmt_args;
        print_uptime(stderr);
        fprintf(stderr, "["  FYELLOW "WARNING" NONE "][" FCYAN "%s" NONE "]: ", type);
        va_start(fmt_args, msg_fmt);
        vfprintf(stderr, msg_fmt, fmt_args);
        va_end(fmt_args);
        fprintf(stderr, "\n");
#endif
}

void info(const char *type, const char *msg_fmt, ...)
{
#ifdef __INFO_ENABLED__
        va_list fmt_args;
        print_uptime(stderr);
        fprintf(stderr, "[" F_LGREEN "INFO" NONE "][" FGREEN "%s" NONE "]: ", type);
        va_start(fmt_args, msg_fmt);
        vfprintf(stderr, msg_fmt, fmt_args);
        va_end(fmt_args);
        fprintf(stderr, "\n");
#endif
}
