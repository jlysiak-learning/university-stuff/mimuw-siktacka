#pragma once

#include "../common/types.h"
#include "../common/defs.h"

/** Client configuration */
typedef struct {
        char player_name[PLAYER_NAME_SIZE + 1];

        /* GUI */
        char ghost[HOST_NAME_SIZE];
        u16 gport;
        u8 gaddr_type;

        /* Game server */
        char shost[HOST_NAME_SIZE];
        u16 sport;
        u8 saddr_type;

} cconf_t;