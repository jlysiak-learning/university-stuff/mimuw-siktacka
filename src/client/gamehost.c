/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Game server communication
 */

// Using -std=c11, gcc can't see struct addrinfo... :(
#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include "gamehost.h"
#include "logs.h"
#include "../common/defs.h"
#include "../common/utils.h"

//==================== MODULE STRUCTURES
typedef struct ghost_t {
        /* Network */
        char host[HOST_NAME_SIZE];
        u16 port;
        u8 addr_type;
        saddr_in addr;  //!< GUI server address structure
        saddr_in6 addr6;  //!< GUI server address structure
        saddr_l addr_l; //!< GUI server address structure length
        int sock;       //!< Gui server socket file descriptor

        /* Communication */
        u8 tx_buff[MAX_CLIENT_DGRAM_SIZE]; //!< Almost prepared datagram buffer,
                                        //!< update of dir and req ev. needed
        u8 tx_buff_l;   //!< Buffer length
        u8 rx_buff[MAX_DGRAM_SIZE];     //!< RX buffer
        u16 rx_buff_l;  //!< Rx buffer length
} ghost_t;

//==================== INTERFACE
ghost_t *ghost_connect(const char *host, u16 port, u8 type, const char *player_name, u64 ss_id)
{
        ghost_t *ghost = calloc(1, sizeof(ghost_t));

        /* Convert host name */
        struct addrinfo addr_hints;
        struct addrinfo *addr_result;
        memset(&addr_hints, 0, sizeof(struct addrinfo));

        addr_hints.ai_family = (type == ADDR_TYPE_IP6 ? AF_INET6 : AF_INET);
        addr_hints.ai_socktype = SOCK_DGRAM;
        addr_hints.ai_protocol = IPPROTO_UDP;
        addr_hints.ai_flags = 0;
        addr_hints.ai_addrlen = 0;
        addr_hints.ai_addr = NULL;
        addr_hints.ai_canonname = NULL;
        addr_hints.ai_next = NULL;
        int retval;
        if ((retval = getaddrinfo(host, NULL, &addr_hints, &addr_result)) != 0)
                fatal("ADDR_INFO", "Convert host/port with getaddrinfo! (msg: %s)", gai_strerror(retval));
        freeaddrinfo(addr_result);

        if (type == ADDR_TYPE_IP6) {
                ghost->sock = socket(AF_INET6, SOCK_DGRAM, 0);
                ghost->addr6.sin6_family = AF_INET6;
                ghost->addr6.sin6_port = htons(port);
                ghost->addr6.sin6_addr = ((saddr_in6 *) (addr_result->ai_addr))->sin6_addr;
                ghost->addr_l = sizeof ghost->addr6;
        } else {
                ghost->sock = socket(AF_INET, SOCK_DGRAM, 0);
                ghost->addr.sin_family = AF_INET;
                ghost->addr.sin_port = htons(port);
                ghost->addr.sin_addr.s_addr = ((saddr_in *) (addr_result->ai_addr))->sin_addr.s_addr;
                ghost->addr_l = sizeof ghost->addr;
        }
        if (ghost->sock < 0)
                fatal_sys("Fail during socket creation.");
        if (connect(ghost->sock, (type == ADDR_TYPE_IP6 ? (saddr *) &ghost->addr6 : (saddr *) &ghost->addr), ghost->addr_l) < 0)
                fatal_sys("Connect with game server failed!");

        /** Prepare datagram data */
        u8 name_l = (u8) strlen(player_name);
        /* This extra line is due to breaking strict-aliasing rule. */
        u64 *ssid = (u64 *) ghost->tx_buff;
        *ssid = htobe64(ss_id);
        memcpy(ghost->tx_buff + 13, player_name, name_l);
        ghost->tx_buff_l = (u8)(13 + name_l);

        info(MSG_TYPE_INIT, "Connected with game server!");
        return ghost;
}

void ghost_close(ghost_t **ghost)
{
        free(*ghost);
        *ghost = NULL;
}

int ghost_sock(ghost_t *ghost)
{
        return ghost->sock;
}

u8 *ghost_recv(ghost_t *ghost, u16 *len)
{
        ssize_t l;
        l = recv(ghost->sock, ghost->rx_buff, MAX_DGRAM_SIZE, 0);
        if (l < 0)
                fatal_sys("UDP recv() error, check whether server is running...");
        ghost->rx_buff_l = (u16) l;
        *len = (u16) l;

        info("UDP RECV", "Received %d bytes!", l);
        dump_data("UDP RECV", ghost->rx_buff, ghost->rx_buff_l);
        return ghost->rx_buff;
}

void ghost_send(ghost_t *ghost, i8 dir, u32 req_ev)
{
        ssize_t retval;
        /* Fill datagram data. */
        ghost->tx_buff[8] = (u8) dir;
        /* This extra line is due to breaking strict-aliasing rule. */
        u32 *req_ev_no = (u32 *) &ghost->tx_buff[9];
        *req_ev_no = htonl(req_ev);
        retval = send(ghost->sock, (void *) ghost->tx_buff, ghost->tx_buff_l, 0);
        if (retval < 0)
                fatal_sys("Datagram sending error!");

        info("UDP SEND", "Datagram sent [%u bytes]: %d, %u", ghost->tx_buff_l, dir, req_ev);
        dump_data("UDP SEND", ghost->tx_buff, ghost->tx_buff_l);
}
