/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client resources.
 */
#pragma once

#include <sys/select.h>
#include "gui.h"
#include "gamehost.h"
#include "event.h"
#include "game.h"
#include "../common/clock.h"

/** Client resources */
typedef struct {
        /* Useful app struct*/
        clk_t *clk;             //!< Application clock
        gui_t *gui;             //!< GUI server
        ghost_t *ghost;         //!< Game server
        game_t *game;

        /* Flow control */
        uint8_t new_msg;        //!< Flag indicates client cyclic duties
        volatile u8 over;       //!< Close our wonderful client?

        /* select() stuff */
        int max_sock;           //!< Greater socket from gsock/ssock
        fd_set rd_sock_set;     //!< Read FD_SET for multiplexed I/O, see select()
} cres_t;