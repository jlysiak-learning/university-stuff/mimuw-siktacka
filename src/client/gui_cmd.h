/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * GUI - client commands definitions.
 */

#pragma once

/* Commands messages */
#define CMD_L_UP        "LEFT_KEY_UP"
#define CMD_L_DW        "LEFT_KEY_DOWN"
#define CMD_R_UP        "RIGHT_KEY_UP"
#define CMD_R_DW        "RIGHT_KEY_DOWN"

/* Commands numbers */
#define CMD_UNKNOW      -1
#define CMD_L_UP_N      0
#define CMD_L_DW_N      1
#define CMD_R_UP_N      2
#define CMD_R_DW_N      3

