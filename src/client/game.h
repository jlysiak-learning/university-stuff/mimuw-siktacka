/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Gamestate module.
 */

#pragma once

#include "../common/types.h"
#include "event.h"

//==================== MODULE STRUCTURES
typedef struct game_t game_t;

//==================== INTERFACE
game_t *game_create();

void game_clean(game_t **game);

/** Returns pointer to player state.
  * It's needed by game_cmd_handler called from gui module */
i8 * game_dir_ptr(game_t *game);

/** GUI command handler to control player state from other module. */
void game_cmd_handler(i8 *dir, i8 cmd);

/** Event number requested from game server. */
u32 game_req_event(game_t *game);

/** Last event sent to gui. */
u32 game_last_sent(game_t *game);

/** Takes all datagram from game server and slices it
 * into events packages and adds to event queue. */
void game_parse_events(game_t *game, u8 *buff, u16 len);

event_t *game_get_next_event(game_t *game);