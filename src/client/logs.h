/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Logger module with exit on fail.
 * Beautiful color logs included.
 *
 * Compile module with specified option to enable functionalities.
 * info logs: -D__INFO_ENABLED__
 * warrnings: -D__WARNING_ENABLED__
 */

#pragma once

#include <stdarg.h>
#include "resources.h"

/** Initialize log module. */
void log_init(void (* clean_f)(cres_t *), cres_t *resources);

/** Fatal error.
 *  Print message, exit with code EXIT_FAILURE. */
void fatal(const char *type, const char *msg_fmt, ...);

/** Fatal system error.
 *  Print error message, exit with code EXIT_FAILURE. */
void fatal_sys(const char *msg_fmt, ...);

/** Warning.
 *  Print message.*/
void warning(const char *type, const char *msg_fmt, ...);

/** Info.
 *  Print message. */
void info(const char *type, const char *msg_fmt, ...);