/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * GUI server communication.
 */

#pragma once

//==================== MODULE STRUCTURES
#include "event.h"

/** GUI host data and communication data buffers. */
typedef struct gui_t gui_t;

//==================== INTERFACE
/** Tries to establish connection with GUI server. */
gui_t *gui_connect(const char *host, u16 port, u8 type, i8 *hnd_arg, void (*cmd_handler)(i8*, i8));

/** Closes connection and frees resources. */
void gui_close(gui_t **gui);

/** Returns active GUI socket. */
int gui_sock(gui_t *gui);

/** Gets next game event and sends it to GUI server. */
void gui_send_msg(gui_t *gui, event_t *ev);

void gui_recv(gui_t *gui);
