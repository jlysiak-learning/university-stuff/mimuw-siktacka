/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Game server communication
 */

#pragma once

#include "../common/types.h"

//==================== MODULE STRUCTURES
typedef struct ghost_t ghost_t;

//==================== INTERFACE
/** Tries to establish connection with game server. */
ghost_t *ghost_connect(const char *host, u16 port, u8 type, const char *player_name, u64 ss_id);

/** Closes connection and frees resources. */
void ghost_close(ghost_t **ghost);

/** Returns active game host socket. */
int ghost_sock(ghost_t *ghost);

/** Receives datagram and returns pointer on data. */
u8 *ghost_recv(ghost_t *ghost, u16 *len);

/** Prepares message to game server and sends it. */
void ghost_send(ghost_t *ghost, i8 dir, u32 req_ev);