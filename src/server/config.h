/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Server configuration.
 */
#pragma once

#include "../common/types.h"

/** Server configuration */
typedef struct {
        u16 area_width;         //!< Game area width in pixels
        u16 area_height;        //!< Height, same as above ^
        u16 rounds_per_sec;     //!< Game speed in rounds per second
        u16 turn_speed;         //!< Gameplay turning speed
        u64 seed;               //!< PRNG seed
        u16 port;               //!< Server port
} sconf_t;