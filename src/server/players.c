/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Player manager module.
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "players.h"
#include "../common/defs.h"
#include "fail_mod.h"

//==================== MODULE DEFS
# define PI		3.14159265358979323846

//==================== MODULE STRUCTURES
typedef struct player_t {
        double hx;      //!< Head position x
        double hy;      //!< Head position y
        double dir;     //!< CCW direction angle in degs, measured from X axis
        i8 last_cmd;    //!< Last received command from player.
        i8 id;          //!< Player id
        char name[PLAYER_NAME_SIZE + 1]; //!< Player name
        u8 name_l;
        /** Is player alive? :)
         * It means players head is still moving,
         * but client can be disconnected. */
        u8 alive;
} player_t;

typedef struct players_t {
        player_t *players[MAX_CLIENTS];
        u8 cnt;
        u8 alive_cnt;
} players_t;

//==================== INTERFACE
players_t *ptab_create()
{
        players_t *ptab = calloc(1, sizeof(players_t));
        return ptab;
}

void ptab_free(players_t **players)
{
        if (!*players)
                return;
        for (u8 i = 0; i < (*players)->cnt; ++i) {
                if ((*players)->players[i]) {
                        free((*players)->players[i]);
                }
        }
        free(*players);
        *players = NULL;
}

player_t *ptab_add(players_t *ptab, const char *name)
{
        if (!ptab || ptab->cnt >= MAX_CLIENTS)
                return NULL;
        player_t *player = calloc(1, sizeof(player_t));
        ptab->players[ptab->cnt] = player;
        player->id = ptab->cnt++;
        player->last_cmd = TURN_DIR_F;
        strcpy(player->name, name);
        player->name_l = (u8) strlen(name);
        player->alive = TRUE;
        ptab->alive_cnt++;
        return player;
}

void ptab_update_dir(player_t *player, i8 dir)
{
        player->last_cmd = dir;
}

char *ptab_names_list(players_t *players, u16 *len)
{
        if (!players)
                return NULL;

        u16 size = 0;
        for (u8 i = 0; i < players->cnt; ++i)
                size += players->players[i]->name_l + 1;
        char *names = calloc(size, sizeof(char));
        char *p = names;
        for (u8 i = 0; i < players->cnt; ++i) {
                player_t *pl = players->players[i];
                memcpy((void *) p, (void *) pl->name, pl->name_l + 1);
                p += pl->name_l + 1;
        }
        *len = size;
        return names;
}

u8 ptab_players_cnt(players_t *players)
{
        return players->cnt;
}

u8 ptab_alive_cnt(players_t *players)
{
        return players->alive_cnt;
}

/** Move player foreward by one.
 * Returns 0, if pixel not changed, 1 otherwise. */
u8 ptab_move(players_t *players, u8 id)
{
        player_t *player = players->players[id];
        i32 old_x = (i32) player->hx;
        i32 old_y = (i32) player->hy;
        player->hx += cos(player->dir * PI / 180.);
        player->hy -= sin(player->dir * PI / 180.);
        i32 new_x = (i32) player->hx;
        i32 new_y = (i32) player->hy;
        warning("POS", "x: %f, y: %f, d: %f", player->hx, player->hy, player->dir);
        return (u8) ((new_x != old_x) || (new_y != old_y));
}

void ptab_change_dir(players_t *players, u8 id, u16 turn_speed)
{
        player_t *p = players->players[id];
        p->dir -= p->last_cmd * turn_speed;
        if (p->dir >= 360.)
                p->dir -= 360.;
        if (p->dir < 0)
                p->dir += 360.;
}

void ptab_get_pos(players_t *players, u8 id, i32 *x, i32 *y)
{
        *x = (i32) players->players[id]->hx;
        *y = (i32) players->players[id]->hy;
}

i8 ptab_get_winner(players_t *players)
{
        for (u8 i = 0; i < players->cnt; ++i) {
                if (!players->players[i]->alive)
                        continue;
                return i;
        }
        return -1;
}

void ptab_kill(players_t *players, u8 id)
{
        players->alive_cnt--;
        players->players[id]->alive = FALSE;
}

u8 ptab_is_alive(players_t *players, u8 id)
{
        return players->players[id]->alive;
}

char *ptab_get_name(players_t *players, u8 id)
{
        return players->players[id]->name;
}

void ptab_set_pos(players_t *players, u8 id, double x, double y, double dir)
{
        players->players[id]->hx = x;
        players->players[id]->hy = y;
        players->players[id]->dir = dir;
}