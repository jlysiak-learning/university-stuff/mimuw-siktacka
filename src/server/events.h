/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Events table manager module.
 * This module holds events in packed form,
 * prepared to send in datagram.
 * Implements:
 *  -   preparation each of game event.
 *  -   packing events into given size buffer
 *  -   creating and freeing
 */

#pragma once

#include "../common/types.h"

//==================== MODULE STRUCTURES

/** Events table */
typedef struct events_t events_t;

//==================== INTERFACE
/** Creates events table of given length.
 * Destroy with ev_free(...)! */
events_t *ev_create(u32 size);

/** Deletes all events in table. */
void ev_clear(events_t *events);

/** Destroys events table. */
void ev_free(events_t **events);

/** Returns length of event table. */
u32 ev_get_len(events_t *events);

/** Pack into buffer as much events as it can hold in given size.
 * Returns bytes count copied to buffer.
 * ev_no is set to next expected event. */
u16 ev_pack_events(events_t *events, u8 *buff, u32 buff_size, u32 game_id, u32 *ev_no);

/** Adds NEW GAME event into table. */
void ev_create_newgame(events_t *events, u32 w, u32 h, const char *names, u16 names_l);

/** Adds PIXEL event into table. */
void ev_create_pixel(events_t *events, u32 x, u32 y, u8 player);

/** Adds PLAYER ELIMINATED event into table. */
void ev_create_plelim(events_t *events, u8 player);

/** Adds GAME OVER event into table. */
void ev_create_gameover(events_t *events);

