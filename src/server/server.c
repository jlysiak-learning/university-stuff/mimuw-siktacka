/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Server entry point.
 */

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>

#include "../common/utils.h"
#include "routines.h"
#include "fail_mod.h"

//====== MODULE VARIABLES
/** Pointer for SIGINT handler to signalize server kill. */
static volatile uint8_t *over;

//====== INTERNAL HELPER METHODS
/** SIGINT handler */
static void sig_int(int sig);

/** Loads and validates server configuration from command line args. */
static void load_config(int argc, char **argv, sconf_t *conf);

/** Print user friendly instructions, how use this app. */
static void print_usage();

//====== IMPLEMENTATION
/** Entry point.. */
int main(int argc, char **argv)
{
        sconf_t conf;
        sres_t *res;

        load_config(argc, argv, &conf);
        res = server_create(&conf);
        /* Provide server resources and cleaning functions to fail mod. */
        fmod_init(server_clean, res);
        /* Let us to softly kill server with Ctrl+C */
        over = &(res->over);
        if (signal(SIGINT, sig_int) == SIG_ERR)
                fatal_sys("Trying to set SIGINT handler.");
        server_run(res);
        server_clean(res);
        return EXIT_SUCCESS;
}

static void load_config(int argc, char **argv, sconf_t *conf)
{

        conf->area_width = DEFAULT_WIDTH;
        conf->area_height = DEFAULT_HEIGHT;
        conf->rounds_per_sec = DEFAULT_GAME_SPEED;
        conf->turn_speed = DEFAULT_TURN_SPEED;
        conf->port = (in_port_t) DEFAULT_SERVER_PORT;
        conf->seed = get_epoch_usec();

        /* This part is ugly but simple. I'm sorry...
         * It's just parsing args from cmdline + simple validation. */
        uint64_t retval;
        int c;
        opterr = 0;
        while ((c = getopt(argc, argv, "W:H:p:s:t:r:")) != -1)
                switch (c) {
                        case 'W':
                                conf->area_width = (uint16_t) validate_uint(optarg);
                                if (conf->area_width > MAX_WIDTH)
                                        fatal(MSG_TYPE_PARAMS, "Area width %u is to large. [max: %u]", conf->area_width,
                                              MAX_WIDTH);
                                break;

                        case 'H':
                                conf->area_height = (uint16_t) validate_uint(optarg);
                                if (conf->area_height > MAX_HEIGHT)
                                        fatal(MSG_TYPE_PARAMS, "Area height %u is to large. [max: %u]",
                                              conf->area_height,
                                              MAX_HEIGHT);
                                break;

                        case 'p':
                                retval = validate_uint(optarg);
                                if (retval > PORT_NUM_MAX)
                                        fatal(MSG_TYPE_PARAMS, "Not correct port number!");
                                conf->port = (in_port_t) retval;

                                break;
                        case 's':
                                conf->rounds_per_sec = (uint16_t) validate_uint(optarg);
                                if (conf->rounds_per_sec > MAX_GAME_SPEED)
                                        fatal(MSG_TYPE_PARAMS, "Game speed %u is to large. [max: %u]",
                                              conf->rounds_per_sec,
                                              MAX_GAME_SPEED);
                                break;

                        case 't':
                                conf->turn_speed = (uint16_t) validate_uint(optarg);
                                if (conf->turn_speed > MAX_TURN_SPEED)
                                        fatal(MSG_TYPE_PARAMS, "Turning speed %u is to large. [max: %u]",
                                              conf->turn_speed,
                                              MAX_TURN_SPEED);
                                break;

                        case 'r':
                                conf->seed = validate_uint(optarg);
                                break;

                        case '?':
                                if (strchr("WHpstr", optopt) != NULL) {
                                        print_usage();
                                        fatal(MSG_TYPE_PARAMS, "Option -%c requires an argument.", optopt);
                                } else if (isprint(optopt)) {
                                        print_usage();
                                        fatal(MSG_TYPE_PARAMS, "Unknown option `-%c'.", optopt);
                                } else {
                                        print_usage();
                                        fatal(MSG_TYPE_PARAMS, "Unknown option character `\\x%x'.", optopt);
                                }
                        default:
                                fatal(MSG_TYPE_PARAMS, "Parser error.");
                }
}

static void print_usage()
{
        fprintf(stderr, "\n\tUsage: ./siktacka-server [-W n] [-H n] [-p n] [-s n] [-t n] [-r n]\n\n");
        fprintf(stderr, "\t-W n: area width in pixel\tdefault: %d\t\tmax: %d\n", DEFAULT_WIDTH, MAX_WIDTH);
        fprintf(stderr, "\t-H n: area height in pixel\tdefault: %d\t\tmax: %d\n", DEFAULT_HEIGHT, MAX_HEIGHT);
        fprintf(stderr, "\t-s n: rounds per second\t\tdefault: %d\t\tmax: %d\n", DEFAULT_GAME_SPEED, MAX_GAME_SPEED);
        fprintf(stderr, "\t-t n: turning speed\t\tdefault: %d\t\tmax: %d\n", DEFAULT_TURN_SPEED, MAX_TURN_SPEED);
        fprintf(stderr, "\t-p n: server port\t\tdefault: %d\n", DEFAULT_SERVER_PORT);
        fprintf(stderr, "\t-r n: seed\t\t\tdefault: time(NULL)\n\n");
}

static void sig_int(int sig)
{
        info(MSG_TYPE_QUIT, "Shutdown server...");
        *over = TRUE;
}