/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Events table manager implementation.
 */

#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include "events.h"
#include "../common/defs.h"

//==================== MODULE DEFS
/** Put 32 bits into buffer in network byte order and shift buffer. */
#define H2N_32(buff, val)       do{ \
                                        *((u32 *) buff) = htonl(val);\
                                        buff += 4;\
                                } while(0)

/** Put byte into buffer and shift. */
#define H2N_8(buff, val)        do{ \
                                        *buff = val;\
                                        buff++; \
                                }while(0)

//==================== MODULE STRUCTURES
/** Game event packed into bytes array. */
typedef struct event_t {
        u8 *content;
        u16 len;
} event_t;

/** Events table */
typedef struct events_t {
        event_t **events;       //!< Events table
        u32 size;               //!< Table max size
        u32 len;                //!< # of events in table
} events_t;

//==================== HELPERS
/** Checks event table size and resize if needed. */
static void ev_check_size(events_t *events)
{
        if (events->len < events->size)
                return;
        u32 new_size = events->size * 2;
        event_t **new_tab = calloc(new_size, sizeof(event_t *));
        memcpy(new_tab, events->events, sizeof(event_t *) * events->len);
        free(events->events);
        events->size = new_size;
        events->events = new_tab;
}

/** Create event structure with given data size.
 * Automatically adds bytes for len, ev_no,
 * ev_type and crc fields. */
static event_t *ev_create_event(u16 size)
{
        event_t *ev = calloc(1, sizeof(event_t));
        ev->len = size + 13;
        ev->content = calloc(size + 13, sizeof(u8));
        return ev;
}

//==================== INTERFACE

events_t *ev_create(u32 size)
{
        events_t *events = calloc(1, sizeof(events_t));
        events->size = size;
        events->events = calloc(size, sizeof(event_t *));
        return events;
}

void ev_clear(events_t *events)
{
        if (!events)
                return;
        for (u32 i = 0; i < events->len; i++) {
                free(events->events[i]->content);
                free(events->events[i]);
                events->events[i] = NULL;
        }
        events->len = 0;
}

void ev_free(events_t **events)
{
        if (!events)
                return;
        ev_clear(*events);
        free((*events)->events);
        free(*events);
        *events = NULL;
}

u32 ev_get_len(events_t *events)
{
        if (!events)
                return 0;
        return events->len;
}

u16 ev_pack_events(events_t *events, u8 *buff, u32 buff_size, u32 game_id, u32 *ev_no)
{
        if (!events)
                return 0;
        event_t *ev;
        u32 *id = (u32 *) buff;
        *id = htonl(game_id);
        buff += 4;
        u16 size = 4;
        for (; *ev_no < events->len; (*ev_no)++) {
                ev = events->events[*ev_no];
                if (buff_size < ev->len)
                        break;
                memcpy((void *) buff, ev->content, ev->len);
                buff_size -= ev->len;
                buff += ev->len;
                size += ev->len;
        }
        return size;
}

void ev_create_newgame(events_t *events, u32 w, u32 h, const char *names, u16 names_l)
{
        if (!events)
                return;
        ev_check_size(events);
        u32 id = events->len++;
        /* Create event with extra space for W, H & names */
        event_t *ev = ev_create_event(8 + names_l);
        events->events[id] = ev;
        u8 *buff = ev->content;
        /* Event length - len field - crc field */
        H2N_32(buff, ev->len - 8); // event len
        H2N_32(buff, id); // event id
        H2N_8(buff, EVT_NEWGAME); // event type
        H2N_32(buff, w); // area width
        H2N_32(buff, h); // area height
        memcpy(buff, names, names_l); // names
        buff += names_l;
        /* Calc CRC32 from begining to end of data. */
        u32 crc = (u32) crc32(0, ev->content, ev->len - 4);
        H2N_32(buff, crc);
}

void ev_create_pixel(events_t *events, u32 x, u32 y, u8 player)
{
        if (!events)
                return;
        ev_check_size(events);
        u32 id = events->len++;
        /* Create event with extra space for W, H & player id */
        event_t *ev = ev_create_event(9);
        events->events[id] = ev;
        u8 *buff = ev->content;
        /* Event length - len field - crc field */
        H2N_32(buff, ev->len - 8); // event len
        H2N_32(buff, id); // event id
        H2N_8(buff, EVT_PIXEL); // event type
        H2N_8(buff, player); // player id
        H2N_32(buff, x); // X position
        H2N_32(buff, y); // Y position
        /* Calc CRC32 from begining to end of data. */
        u32 crc = (u32) crc32(0, ev->content, ev->len - 4);
        H2N_32(buff, crc);
}

void ev_create_plelim(events_t *events, u8 player)
{
        if (!events)
                return;
        ev_check_size(events);
        u32 id = events->len++;
        /* Create event with extra space for player id */
        event_t *ev = ev_create_event(1);
        events->events[id] = ev;
        u8 *buff = ev->content;
        /* Event length - len field - crc field */
        H2N_32(buff, ev->len - 8); // event len
        H2N_32(buff, id); // event id
        H2N_8(buff, EVT_PLELIM); // event type
        H2N_8(buff, player); // player id
        /* Calc CRC32 from begining to end of data. */
        u32 crc = (u32) crc32(0, ev->content, ev->len - 4);
        H2N_32(buff, crc);
}

void ev_create_gameover(events_t *events)
{
        if (!events)
                return;
        ev_check_size(events);
        u32 id = events->len++;
        /* Create event with extra space for player id */
        event_t *ev = ev_create_event(0);
        events->events[id] = ev;
        u8 *buff = ev->content;
        /* Event length - len field - crc field */
        H2N_32(buff, ev->len - 8); // event len
        H2N_32(buff, id); // event id
        H2N_8(buff, EVT_GAMEOVER); // event type
        /* Calc CRC32 from begining to end of data. */
        u32 crc = (u32) crc32(0, ev->content, ev->len - 4);
        H2N_32(buff, crc);
}

