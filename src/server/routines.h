/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Server main routines.
 */

#pragma once

#include "resources.h"
#include "config.h"

/** Initializes server structures,
 * prepare clock, socket and game. */
sres_t * server_create(sconf_t *conf);

/** Main server procedure. */
void server_run(sres_t *res);

/** Free allocated memory, etc. */
void server_clean(sres_t *res);
