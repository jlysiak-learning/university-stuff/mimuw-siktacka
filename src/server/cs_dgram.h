/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Incoming datagram structure with sender address.
 */
#pragma once

#include "../common/defs.h"
#include "config.h"

/** Client - server datagram + sender address */
typedef struct {
        u64 ss_id;      //!< Client session id
        i8 dir;         //!< Turning direction
        u32 next_ev;    //!< Next expected event
        char name[PLAYER_NAME_SIZE + 1]; //!< client name, C-like string
        u8 name_l;       //!< Name length

        saddr_in6 addr;  //!< Sender address
        saddr_l addr_l; //!< Address len
} cs_dgram_t;
