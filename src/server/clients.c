/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client table implementation.
 */

#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include "fail_mod.h"
#include "clients.h"

//==================== MODULE DEFS
#define CTAB_INFO       "CTAB"

//==================== MODULE STRUCTURES
typedef struct client_t {
        i8 id;          //!< Client id = position in client array, -1 means free slot
        u64 last_up;    //!< Last message timestamp
        u64 ss_id;      //!< Player session id
        char name[PLAYER_NAME_SIZE + 1]; //!< Null byte ended name
        u8 name_l;      //!< Name length. If zero, it's observer!
        u32 req_ev;     //!< Requested event
        i8 turn_dir;    //!< Active turning direction
        u8 rdy_sig;     //!< Ready signal, used in players sync.

        player_t *player;      //!< Pointer to corresponding player in active game

        saddr_in6 cli_addr;      //!< Player's socket
        saddr_l cli_addr_l;     //!< Socket length
} client_t;

/** Clients set. */
typedef struct clients_t {
        client_t clients[MAX_CLIENTS];  //!< Clients table
        u8 connected;   //!< # of connected players
        u8 active;      //!< # of players (named clients), complement -> observers
} clients_t;

//==================== MODULE IMPLEMENTATION

clients_t *ctab_init()
{
        clients_t *clients = calloc(1, sizeof(clients_t));
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; i++, ctab++) {
                ctab->id = CLIENT_FREE_SLOT;
        }
        return clients;
}

void ctab_free(clients_t **clients)
{
        if (!clients)
                return;
        free(*clients);
        *clients = NULL;
}

/** Finds free slot or returns -1 if there's not any. */
static i8 ctab_get_free_id(clients_t *clients)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; ++i, ctab++) {
                if (ctab->id == CLIENT_FREE_SLOT)
                        return i;
        }

        info(CTAB_INFO, "Clients table is full!");
        return CLIENT_FREE_SLOT;
}

/** Compares clients lexicographically.
 * Helper function passed to qsort.
 * 'strcmp' adaper. */
static int ctab_cmp_clients(const void *a, const void *b)
{
        client_t **c1 = (client_t **) a;
        client_t **c2 = (client_t **) b;
        return strcmp((*c1)->name, (*c2)->name);
}

const char *ctab_get_name(clients_t *clients, u8 id)
{
        if (id >= MAX_CLIENTS || clients->clients[id].id == CLIENT_FREE_SLOT)
                return NULL;
        return clients->clients[id].name;
}

void ctab_check_timeouts(clients_t *clients, u64 time_now)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; i++, ctab++) {
                if ((ctab->id == CLIENT_FREE_SLOT) || ((time_now - ctab->last_up) < MAX_TIMEOUT))
                        continue;
                ctab_disconnect(clients, i);
        }
}

i8 ctab_find_sock(clients_t *clients, saddr_in6 *addr)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; i++, ctab++) {
                if (ctab->id == CLIENT_FREE_SLOT)
                        continue;
                if (memcmp(&(ctab->cli_addr.sin6_addr), &(addr->sin6_addr), sizeof(ctab->cli_addr.sin6_addr)))
                        continue;
                if (ctab->cli_addr.sin6_port != addr->sin6_port)
                        continue;
                return i;
        }
        return CLIENT_FREE_SLOT;
}

i8 ctab_find_name(clients_t *clients, const char *name)
{
        /* If unnamed, exit. */
        if (!strlen(name))
                return CLIENT_FREE_SLOT;
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; i++, ctab++) {
                if (ctab->id == CLIENT_FREE_SLOT || !ctab->name_l)
                        continue;
                if (!strcmp(ctab->name, name))
                        return i;
        }
        return CLIENT_FREE_SLOT;
}

u64 ctab_get_ss_id(clients_t *clients, u8 id)
{
        if (id >= MAX_CLIENTS || clients->clients[id].id == CLIENT_FREE_SLOT)
                return 0;
        return clients->clients[id].ss_id;
}

u8 ctab_all_ready(clients_t *clients)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; i++, ctab++) {
                if (ctab->id == CLIENT_FREE_SLOT || !ctab->name_l)
                        continue;
                if (!ctab->rdy_sig)
                        return FALSE;
        }
        return TRUE;

}

void ctab_reset(clients_t *clients)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; ++i, ctab++) {
                ctab->player = NULL;
                ctab->rdy_sig = FALSE;
        }
}

void ctab_reset_ev(clients_t *clients)
{
        u8 i = 0;
        client_t *ctab = clients->clients;
        for (; i < MAX_CLIENTS; ++i, ctab++)
                ctab->req_ev = 0;
}


void ctab_disconnect(clients_t *clients, u8 id)
{
        client_t *cli = &clients->clients[id];
        clients->connected--;
        if (cli->name_l)
                clients->active--;
        cli->id = CLIENT_FREE_SLOT;
        cli->player = NULL;
}

void ctab_update(clients_t *clients, u8 id, u32 req_ev, i8 dir, u64 time_now)
{
        if (id >= MAX_CLIENTS || clients->clients[id].id == CLIENT_FREE_SLOT)
                return;
        client_t *cli = &clients->clients[id];
        cli->last_up = time_now;
        cli->turn_dir = dir;
        cli->req_ev = req_ev;

        if (dir != TURN_DIR_F && !cli->rdy_sig && cli->name_l) {
                cli->rdy_sig = TRUE;
                info(CTAB_INFO, "Client %s[id: %u] is ready!", cli->name, cli->id);
        }

        if (cli->player)
                ptab_update_dir(cli->player, dir);
}

i8 ctab_connect(clients_t *clients, const char *name, u64 ss_id, saddr_in6 *addr, u64 time_now)
{
        i8 id;
        if ((id = ctab_get_free_id(clients)) < 0)
                return CLIENT_FREE_SLOT;
        client_t *cli;
        cli = &clients->clients[id];

        cli->id = id;
        cli->last_up = time_now;
        cli->turn_dir = TURN_DIR_F;
        cli->req_ev = 0;
        cli->rdy_sig = FALSE;
        cli->ss_id = ss_id;
        cli->name_l = (u8) strlen(name);
        strcpy(cli->name, name);

        cli->cli_addr_l = sizeof(*addr);
        memcpy((void *) &cli->cli_addr, (void *) addr, cli->cli_addr_l);

        if (cli->name_l)
                clients->active++;
        clients->connected++;

        info(CTAB_INFO, "Client [name: %s, id: %d] connected!.",cli->name, id);
        return id;
}

saddr_in6 *ctab_get_sock(clients_t *clients, u8 id)
{
        if (id >= MAX_CLIENTS || clients->clients[id].id == CLIENT_FREE_SLOT)
                return NULL;
        return &clients->clients[id].cli_addr;
}

players_t *ctab_create_ptable(clients_t *clients)
{
        u8 cnt = 0;
        client_t **tmp_ctab = calloc(MAX_CLIENTS, sizeof(client_t *));
        u8 i = 0;
        client_t *ctab = clients->clients;
        /* Build named clients table. */
        for (; i < MAX_CLIENTS; i++, ctab++)
                if (ctab->name_l)
                        /* It's player! */
                        tmp_ctab[cnt++] = ctab;
        /* Sort clients pointers. */
        qsort((void *) tmp_ctab, cnt, sizeof(client_t **), ctab_cmp_clients);
        /* Create player table. */
        players_t *ptab = ptab_create();
        /* Iterate over selected clients and add them to players table.
         * Clients are sorted lexicographically.
         * In same moment bind player structure to corresponding client. */
        for (i = 0; i < cnt; ++i)
                tmp_ctab[i]->player = ptab_add(ptab, tmp_ctab[i]->name);
        free(tmp_ctab);
        return ptab;
}

u8 ctab_active_cnt(clients_t *clients)
{
        if (!clients)
                return 0;
        return clients->active;
}

u8 ctab_connected(clients_t *clients)
{
        return clients->connected;
}

u32 ctab_next_event(clients_t *clients, u8 id)
{
        return clients->clients[id].req_ev;
}

void ctab_set_next_event(clients_t *clients, u8 id, u32 next)
{
        clients->clients[id].req_ev = next;
}