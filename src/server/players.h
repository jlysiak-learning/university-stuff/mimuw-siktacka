/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Player manager module.
 */

#pragma once

#include "../common/types.h"

//==================== MODULE STRUCTURES
/** In-game player state */
typedef struct player_t player_t;

/** Players table. */
typedef struct players_t players_t;

//==================== INTERFACE
/** Creates players table.
 * Destroy structure with ptab_free(...). */
players_t *ptab_create();

/** Destroys players table.
 * DANGER! Always reset clients before freeing players! */
void ptab_free(players_t **players);

/** Adds player to players table and returns its pointer. */
player_t *ptab_add(players_t *ptab, const char *name);

/** Updates player's direction.
 * It's called from client update function. */
void ptab_update_dir(player_t *player, i8 dir);

/** Changes player direction and keeps direction angle in [0, 360[ range. */
void ptab_change_dir(players_t *players, u8 id, u16 turn_speed);

/** Creates string with player names.
 * Format: [Player1\0][Player2\0]...[PlayerN\0]
 * You're responsible for freeing allocated memory. */
char *ptab_names_list(players_t *players, u16 *len);

/** Returns number of players in the game. */
u8 ptab_players_cnt(players_t *players);

/** Returns number of still alive players in the game. */
u8 ptab_alive_cnt(players_t *players);

/** Moves given players by one.
 * Returns TRUE if head's pixel has changed. */
u8 ptab_move(players_t *players, u8 id);

/** Kills player. :( */
void ptab_kill(players_t *players, u8 id);

void ptab_set_pos(players_t *players, u8 id, double x, double y, double dir);

void ptab_get_pos(players_t *players, u8 id, i32 *x, i32 *y);

/** Checks the winner and announces it's name in log. */
i8 ptab_get_winner(players_t *players);

char *ptab_get_name(players_t *players, u8 id);

u8 ptab_is_alive(players_t *players, u8 id);
