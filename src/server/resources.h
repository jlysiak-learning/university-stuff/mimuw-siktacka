/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Server structures.
 */

#pragma once

#include <sys/select.h>
#include "clients.h"
#include "gameplay.h"
#include "cs_dgram.h"
#include "../common/defs.h"
#include "../common/clock.h"

/** Server resources */
typedef struct {
        /* Game logic & timming */
        clients_t *clients;     //!< Connected clients
        game_t* game;           //!< Game
        clk_t *clk;             //!< Application clock

        /* Networking */
        int sock;               //!< Server socket file descriptor
        fd_set sock_set;        //!< FD_SET for multiplexed I/O, see select()
        cs_dgram_t recv_dgram;  //!< Recently received datagram from client
        u8 buff[MAX_DGRAM_SIZE];//!< Datagram operational buffer
        u16 buff_l;             //!< Buffer len
        u8 req_pending;         //!< Flag set if any of client needs events

        /* Main loop control */
        volatile u8 over;       //!< It's over? ;(
} sres_t;