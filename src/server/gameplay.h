/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Gameplay module.
 */

#pragma once

#include "../common/types.h"
#include "clients.h"

typedef struct game_t game_t;

//==================== INTERFACE
/** Create gameplay structure. */
game_t * game_create(clients_t *clients, u32 width, u32 heigth, u64 seed, u16 turn_speed);

/** Destroys game. */
void game_free(game_t **game);

/** Update game. */
void game_update(game_t *game);

/** Returns pointer to new round flag. */
u8 *game_round_flag(game_t *game);

/** Checks whether game is running... */
u8 game_is_running(game_t *game);

/** Returns new round flag status. */
u8 game_is_new_round_set(game_t *game);

u32 game_events_cnt(game_t *game);

/** Package for event_s module method, see ev_pack_events(...). */
u16 game_pack_events(game_t *game, u8 *buff, u32 buff_size, u32 *ev_no);
