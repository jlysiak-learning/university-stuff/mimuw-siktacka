/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Gameplay module.
 */

#include <stdlib.h>
#include "gameplay.h"
#include "fail_mod.h"
#include "events.h"

//==================== MODULE DEFS
#define GAMEPLAY_INFO           "GAME"

#define GAMESTATE_PREPARE       0
#define GAMESTATE_RUNNING       1

//==================== MODULE STRUCTURES
/** Game state data structure */
typedef struct game_t {
        u64 rand;               //!< PRNG
        u16 turn_speed;         //!< Turning speed
        u32 heigth;             //!< Arena heigth
        u32 width;              //!< Arena width

        u32 game_id;            //!< Game ID
        u32 round_cnt;          //!< # of rounds
        u8 new_round;           //!< New round indicator, set to TRUE by clock
        u8 game_state;          //!< Active game state

        i8 **arena;             //!< Game arena, -1 free pixel, k => 0 active player id

        players_t *players;     //!< Players in the game
        clients_t *clients;     //!< Clients

        events_t *events;       //!< Game events
} game_t;


//==================== HELPER METHODS
/** Next "random" number. */
static u64 next_rand(game_t *game)
{
        u64 old = game->rand;
        game->rand = (game->rand * RNG_MUL) % RNG_MOD;
        return old;
}

/** Clears all arena. */
static void clear_arena(game_t *game)
{
        for (u32 i = 0; i < game->width; ++i) {
                for (u32 j = 0; j < game->heigth; ++j) {
                        game->arena[i][j] = ARENA_FREE;
                }
        }
}

/** Eliminates player from the game, generate PLAYER_ELIMINATED event. */
static void kill_player(game_t *game, u8 id)
{
        ptab_kill(game->players, id);
        ev_create_plelim(game->events, id);
        info(MSG_TYPE_GAME, "Player %s eliminated!", ptab_get_name(game->players, id));
}

/** Game is over. Change game state.
 * Reset clients' ready signal indicators.
 * If logger is enabled, announces the winner. :)*/
static void end_game(game_t *game)
{
        i8 id;
        if ((id = ptab_get_winner(game->players)) != -1)
                info(MSG_TYPE_GAME, "Game is over! Player %s won! :)", ptab_get_name(game->players, (u8) id));
        ev_create_gameover(game->events);
        ctab_reset(game->clients); // Here order is important
        ptab_free(&game->players);
        game->game_state = GSTATE_PREPARE;
}

/** Checks given pixel on game arena and sets player's ID.
 * New pixel is set only when it's free and ARENA_FREE number is returned.
 * Otherwise, functions returns already set id in given pixel.
 * x, y range must be checked before using this function.
 * This function can be used to detect who killed given player. */
static i8 set_pixel(i8 **arena, u32 x, u32 y, u8 player)
{
        if (arena[x][y] < 0) {
                arena[x][y] = player;
                return ARENA_FREE;
        } else {
                return arena[x][y];
        }
}

/** Validates player position within game arena.
 * Returns FALSE, if position is already set by other player
 * or players hits the wall.
 * Returns TRUE, if position is valid. */
static u8 check_position(game_t *game, u8 id, i32 x, i32 y)
{
        if (x < 0 || x >= game->width) // Hits the walls?
                return FALSE;
        if (y < 0 || y >= game->heigth) // Hits the walls?
                return FALSE;
        /* set_pixel(x ,y) < 0 means (x, y) pixel was free, see implementation above. */
        return (u8) (set_pixel(game->arena, (u32) x, (u32) y, id) < 0);
}

/** Checks whether game is over.
 * Returns TRUE if yes, FALSE if not yet. :) */
static u8 is_game_over(game_t *game)
{
        /* Not yet... ? */
        if (ptab_alive_cnt(game->players) > 1)
                return FALSE;
        /* Definitely, it's over... */
        end_game(game);
        return TRUE;
};

/** Makes one step in the game - round.
 * See gameplay specification for more info. */
static void new_round(game_t *game)
{
        info(MSG_TYPE_GAME, "New round began: %u", game->round_cnt++);

        u8 i;
        u8 cnt = ptab_players_cnt(game->players);
        i32 x, y;
        for (i = 0; i < cnt; i++) {
                if (!ptab_is_alive(game->players, i))
                        continue;
                ptab_change_dir(game->players, i, game->turn_speed);
                if (!ptab_move(game->players, i))
                        continue;
                ptab_get_pos(game->players, i, &x, &y);
                if (check_position(game, i, x, y))
                        ev_create_pixel(game->events, (u32) x, (u32) y, i);
                else {
                        ev_create_plelim(game->events, i);
                        kill_player(game, i);
                        if (is_game_over(game))
                                break;
                }
        }
        game->new_round = FALSE;
}

/** Initializes players' heads positions and directions.
 * See gameplay specification for more info. */
static void init_positions(game_t *game)
{
        double x, y, d;
        u8 cnt = ptab_players_cnt(game->players);
        for (u8 i = 0; i < cnt; ++i) {
                x = next_rand(game) % game->width + 0.5;
                y = next_rand(game) % game->heigth + 0.5;
                d = next_rand(game) % 360;
                if (set_pixel(game->arena, (u32) x, (u32) y, i) < 0) {
                        ev_create_pixel(game->events, (u32) x, (u32) y, i);
                        ptab_set_pos(game->players, i, x, y, d);
                } else {
                        kill_player(game, i);
                        /* This check is required in case of
                         * very unlucky rand() sequence. */
                        if (is_game_over(game))
                                break;
                }
        }
}

/** Prepares new game, inits structs, players, game arena
 * and generate NEW_GAME event. */
static void begin_game(game_t *game)
{
        info(MSG_TYPE_GAME, "Begining new game...");
        /* Create players table. */
        game->players = ctab_create_ptable(game->clients);
        clear_arena(game);
        game->game_id = (u32) next_rand(game);
        game->game_state = GSTATE_RUNNING;
        game->round_cnt = 0;
        game->new_round = FALSE;
        ev_clear(game->events);
        ctab_reset_ev(game->clients);
        u8 connected = ctab_connected(game->clients);
        u8 in_game = ptab_alive_cnt(game->players);
        info(MSG_TYPE_GAME, "Let's the battle begin!");
        info(MSG_TYPE_GAME, "# of players: %u, # of observers: %u",
             in_game, connected - in_game);

        u16 len;
        char *names = ptab_names_list(game->players, &len);
        ev_create_newgame(game->events, game->width, game->heigth, names, len);
        free(names);
        init_positions(game);
}
//==================== INTERFACE

game_t *game_create(clients_t *clients, u32 width, u32 height, u64 seed, u16 turn_speed)
{
        game_t *game = calloc(1, sizeof(game_t));
        game->width = width;
        game->heigth = height;
        game->rand = seed;
        game->turn_speed = turn_speed;
        game->clients = clients;
        game->game_state = GAMESTATE_PREPARE;

        game->arena = calloc(width, sizeof(i8 *));
        if (!game->arena)
                fatal_sys("Game arena calloc() error!");
        for (u32 i = 0; i < width; ++i) {
                game->arena[i] = calloc(height, sizeof(i8));
                if (!game->arena[i])
                        fatal_sys("Game arena[i] calloc() error!");
        }
        /* Set init size of event queue to 1/10 of arena area.
         * In most games this should be enough. */
        u32 init_size = width * height / 10;
        game->events = ev_create(init_size);
        game->players = NULL;
        return game;
}

void game_free(game_t **game)
{
        if (!*game)
                return;

        ev_free(&(*game)->events);
        ctab_reset((*game)->clients);
        ptab_free(&(*game)->players);
        free(*game);
        *game = NULL;
}

void game_update(game_t *game)
{
        switch (game->game_state) {
                case GAMESTATE_PREPARE:
                        /* Check whether each player send signal to start, then begin game. */
                        if (ctab_all_ready(game->clients) && ctab_active_cnt(game->clients) > 1)
                                begin_game(game);
                        break;

                case GAMESTATE_RUNNING:
                        /* If possible, do one step.
                         * This will be executed with given 'rounds_per_seconds' freq. */
                        if (game->new_round)
                                new_round(game);
                        break;

                default:
                        /* This should never happen... */
                        fatal(GAMEPLAY_INFO, "Undefined game state!");
        }
}

u8 *game_round_flag(game_t *game)
{
        if (!game)
                return NULL;
        return &game->new_round;
}

u8 game_is_running(game_t *game)
{
        return (u8) (game->game_state == GAMESTATE_RUNNING);
}

u8 game_is_new_round_set(game_t *game)
{
        return game->new_round;
}

u32 game_events_cnt(game_t *game)
{
        return ev_get_len(game->events);
}

u16 game_pack_events(game_t *game, u8 *buff, u32 buff_size, u32 *ev_no)
{
        return ev_pack_events(game->events, buff, buff_size, game->game_id, ev_no);
}
