/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Client manager module.
 */

#pragma once

#include "../common/types.h"
#include "../common/defs.h"
#include "players.h"

//==================== MODULE STRUCTURES
/** Clients set. */
typedef struct clients_t clients_t;

//==================== INTERFACE
/** Initializes clients structs. */
clients_t * ctab_init();

/** Destroy clients table. */
void ctab_free(clients_t **clients);

/** Iterates over all connected clients.
 * Disconnect if no response in last MAX_TIMEOUT. */
void ctab_check_timeouts(clients_t *clients, u64 time_now);

/** Disconnect client with given id. */
void ctab_disconnect(clients_t *clients, u8 id);

/** Tires to find sender socket among clients.
 * If socket match, returns client id,
 * -1 if it's new client. */
i8 ctab_find_sock(clients_t *clients, saddr_in6 *addr);

/** Find client with given name. Return named client id if names matched.
 * Returns -1 if name has zero length or such client doesn't exist.*/
i8 ctab_find_name(clients_t * clients, const char *name);

/** Tries to connect new client to server.
 * If client connected successfully, returns its id.
 * Otherwise, -1. */
i8 ctab_connect(clients_t *clients, const char *name, u64 ss_id, saddr_in6 *addr, u64 time_now);

/** Returns client's session id */
u64 ctab_get_ss_id(clients_t *clients, u8 id);

/** Get next client's socket.
 * Proto-iterator over client collection. */
saddr_in6 *ctab_get_sock(clients_t *clients, u8 id);

/** Updates client state. */
void ctab_update(clients_t *clients, u8 id, u32 req_ev, i8 dir, u64 time_now);

/** Check whether all named clients are ready to play next game.
 * Returns TRUE or FALSE. */
u8 ctab_all_ready(clients_t *clients);

u8 ctab_active_cnt(clients_t *clients);

/** Resets all clients state. */
void ctab_reset(clients_t *clients);

/** Prepares ordered players set to use it in the game. */
players_t * ctab_create_ptable(clients_t *clients);

/** Returns total number of connected clients. */
u8 ctab_connected(clients_t *clients);

/** Gets next event number */
u32 ctab_next_event(clients_t *clients, u8 id);

/** Sets requested event to zero. */
void ctab_reset_ev(clients_t *clients);