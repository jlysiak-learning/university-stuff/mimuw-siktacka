/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Server main routines.
 */

#define _DEFAULT_SOURCE
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "routines.h"
#include "clients.h"
#include "fail_mod.h"
#include "resources.h"
#include "cs_dgram.h"
#include "config.h"
#include "../common/utils.h"

//==================== MODULE DEFS
/** Put 64 bits into buffer in host byte order and shift buffer. */
#define N2H_64(val, buff)       do{ \
                                        val = be64toh(*((u64 *) buff));\
                                        buff += 8;\
                                } while(0)

/** Put 32 bits into buffer in host byte order and shift buffer. */
#define N2H_32(val, buff)       do{ \
                                        val = ntohl(*((u32 *) buff));\
                                        buff += 4;\
                                } while(0)

/** Put byte into buffer and shift. */
#define N2H_8(val, buff)        do{ \
                                        val = *buff;\
                                        buff++; \
                                }while(0)


//==================== HELPERS
/** Network stuff initializations. */
static void server_init_net(sres_t *res, u16 port)
{
        saddr_in6 addr;
        saddr_l addr_len;
        res->sock = socket(AF_INET6, SOCK_DGRAM, 0);
        if (res->sock < 0)
                fatal_sys("Fail during socket creation.");
        addr.sin6_family = AF_INET6;
        addr.sin6_port = htons(port);
        addr.sin6_addr = in6addr_any;
        addr_len = sizeof(addr);
        if (bind(res->sock, (saddr *) &addr, addr_len) < 0)
                fatal_sys("Bind fail!");
}

/** Validates datagram content.
 * Returns 1, if correct. 0 otherwise. */
static u8 validate_content(cs_dgram_t *dgram, u32 ev_q_len)
{
        if (dgram->dir < TURN_DIR_L || dgram->dir > TURN_DIR_R)
                return FALSE;
        if (dgram->next_ev > ev_q_len)
                return FALSE;
        for (u8 i = 0; i < dgram->name_l; ++i) {
                if (dgram->name[i] < PLAYER_CHAR_MIN || PLAYER_CHAR_MAX < dgram->name[i])
                        return FALSE;
        }
        return TRUE;
}

/** Checks received datagram source, validate it
 * and if it's correct, update client state. */
static void process_recv_dgram(sres_t *res)
{
        i8 id;
        u64 ss_id;
        cs_dgram_t *dgram = &res->recv_dgram;
        if (!validate_content(dgram, game_events_cnt(res->game)))
                return;
        if ((id = ctab_find_sock(res->clients, &dgram->addr)) < 0) {
                if (ctab_find_name(res->clients, dgram->name) < 0) {
                        id = ctab_connect(res->clients, dgram->name, dgram->ss_id, &dgram->addr, clk_now(res->clk));
                }
        } else {
                /* Data from known socket.
                 * If session id match, update client state. */
                ss_id = ctab_get_ss_id(res->clients, id);
                if (ss_id == dgram->ss_id)
                        ctab_update(res->clients, id, dgram->next_ev, dgram->dir, clk_now(res->clk));
                else if (ss_id < dgram->ss_id) {
                        /* Received session id is greater, disconnect and connect again */
                        ctab_disconnect(res->clients, id);
                        ctab_connect(res->clients, dgram->name, dgram->ss_id, &dgram->addr, clk_now(res->clk));
                }
        }
}

/** Unpacks received datagram data stored in buffer
 * into client/server datagram structure.
 * See protocol specification for more info. */
static void unpack_cs_dgram(uint8_t *buff, ssize_t len, cs_dgram_t *dgram)
{
        N2H_64(dgram->ss_id, buff);
        N2H_8(dgram->dir, buff);
        N2H_32(dgram->next_ev, buff);
        u8 left =  (u8) (len - 13);
        memcpy(dgram->name, buff, left);
        dgram->name[left] = 0;
        dgram->name_l = left;
}

static u8 recv_dgram(sres_t *res)
{
        cs_dgram_t *dgram = &res->recv_dgram;
        ssize_t len;
        dgram->addr_l = sizeof(dgram->addr);
        len = recvfrom(res->sock, res->buff, MAX_DGRAM_SIZE, 0, (saddr*) &dgram->addr, &dgram->addr_l);
        if (len < 0) {
                fatal_sys("recvfrom() error");
        } else if (len < MIN_CLIENT_DGRAM_SIZE || MAX_CLIENT_DGRAM_SIZE < len)
                return FALSE;

        unpack_cs_dgram(res->buff, len, dgram);
        return TRUE;
}

/** Checks data availability, sleeps if can
 * and reads datagram if any available. */
static void server_recv(sres_t *res)
{
        struct timeval timeout;
        int retval;
        FD_ZERO(&res->sock_set);
        FD_SET(res->sock, &res->sock_set);
        /* Program cannot wait for data if any job is pending or game should be updated now.
         * then select() checks if any data is available and return immediately.
         * Otherwise, there's nothing to do so server goes sleep on select()
         * to match exactly timming requirements. */
        if (game_is_running(res->game) || res->req_pending) {
                timeout.tv_sec = 0;
                timeout.tv_usec = 0;
                if (!game_is_new_round_set(res->game) && !res->req_pending) {
                        clk_update(res->clk);
                        timeout.tv_usec = clk_left(res->clk) / 1000;
                }
                retval = select(res->sock + 1, &res->sock_set, 0, 0, &timeout);
        } else {
                /* Game is not running. There is no need to do anything
                 * except receiving datagrams from clients. Put sleep process forever.*/
                retval = select(res->sock + 1, &res->sock_set, 0, 0, NULL);
                clk_update(res->clk);
        }
        if (retval < 0) {
                /* Check whether signal caused return from select() */
                if (errno != EINTR)
                        fatal_sys("Fail on select.");
                else {
                        server_clean(res);
                        exit(EXIT_SUCCESS);
                }
        } else if (retval > 0) {
                if (recv_dgram(res))
                        process_recv_dgram(res);
        }
}

static void send_dgram(int sock, u8 *buff, u16 size, saddr_in6 *addr)
{
        ssize_t retval;
        retval = sendto(sock, (void *) buff, size, 0,(saddr *) addr, sizeof(*addr));
        if (retval < 0) {
                fatal_sys("Datagram sending error!");
        }
}

/** Packs data from structures into datagrams and send. */
static void server_send(sres_t *res)
{
        u32 next_ev, ev_cnt;
        saddr_in6 *addr = NULL;
        ev_cnt = game_events_cnt(res->game);
        res->req_pending = FALSE;
        u8 id = 0;
        for (; id < MAX_CLIENTS; ++id) {
                addr = ctab_get_sock(res->clients, id);
                if (addr != NULL) {
                        next_ev = ctab_next_event(res->clients, id);
                        if (next_ev < ev_cnt) {
                                res->buff_l = game_pack_events(res->game, res->buff, MAX_DGRAM_SIZE, &next_ev);
                                send_dgram(res->sock, res->buff, res->buff_l, addr);
                                if (next_ev < ev_cnt)
                                        res->req_pending = TRUE;
                        }
                }
        }
}

//==================== INTERFACE
sres_t *server_create(sconf_t *conf)
{
        info(MSG_TYPE_INIT, "Server init...");
        sres_t *res = calloc(1, sizeof(sres_t));
        res->over = FALSE;
        res->clients = ctab_init();
        res->game = game_create(res->clients, conf->area_width, conf->area_height, conf->seed, conf->turn_speed);
        res->clk = clk_create(NANO_IN_SEC / conf->rounds_per_sec, game_round_flag(res->game));
        server_init_net(res, conf->port);
        info(MSG_TYPE_INIT, "Server init done!");
        return res;
}

void server_run(sres_t *res)
{
        while (!res->over) {
                server_recv(res);
                clk_update(res->clk);
                ctab_check_timeouts(res->clients, clk_now(res->clk));
                game_update(res->game);
                server_send(res);
        }
}

void server_clean(sres_t *res)
{
        if (!res)
                return;
        info(MSG_TYPE_FREE, "Clean and exit...");
        ctab_reset(res->clients);
        ctab_free(&res->clients);
        clk_free(&res->clk);
        game_free(&res->game);
        free(res);
}

