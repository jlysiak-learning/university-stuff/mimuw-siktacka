/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Set of useful functions.
 */

#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <ctype.h>
#include "../server/fail_mod.h"
#include "utils.h"


int validate_int(const char *str)
{
        int i = 0;
        if (str[0] == '-')
                i++;
        for (; i < strlen(str); ++i) {
                if (!isdigit(str[i])) {
                        fatal(MSG_TYPE_PARAMS, "Not a number!");
                }
        }
        return atoi(str);
}

uint64_t validate_uint(const char *str)
{
        int i = 0;
        for (; i < strlen(str); ++i) {
                if (!isdigit(str[i])) {
                        fatal(MSG_TYPE_PARAMS, "Number is not an unsigned!");
                }
        }
        return (uint64_t) atoll(str);
}

int get_host_data(char *data, char *hbuff, char *pbuff)
{
        char *pos = strchr(data, ':');
        char *posr = strrchr(data, ':');

        if (pos != posr) { // IPv6 with default port
                strncpy(hbuff, data, HOST_NAME_SIZE);
                pbuff[0] = 0;
                return ADDR_TYPE_IP6;
        }
        if (pos != NULL) { // IPv4 with port
                *pos = 0;
                pos++;
                if (strlen(pos) > 5)
                        fatal(MSG_TYPE_PARAMS, "Port number is too large!");
                strncpy(pbuff, pos, 6);
        } else {
                pbuff[0] = 0;
        }
        strncpy(hbuff, data, HOST_NAME_SIZE);
        return ADDR_TYPE_IP4;
}

uint64_t get_nanosec()
{
        struct timespec clk;
        if (clock_gettime(CLOCK_MONOTONIC, &clk) < 0) {
                fatal_sys("clock_gettime fail!");
        }
        return (uint64_t) clk.tv_sec * NANO_IN_SEC + clk.tv_nsec;
}

uint64_t get_epoch_usec()
{
        struct timeval tm;
        if (gettimeofday(&tm, NULL) < 0)
                fatal_sys("gettimeofday() failed...");
        return (uint64_t) tm.tv_sec * MICRO_IN_SEC + tm.tv_usec;
}

void dump_data(const char *name, uint8_t *data, size_t len)
{
#ifdef __DUMP_ENABLED__
        printf("\033[93m[%s HEXDUMP]\033[0m\n", name);
        for (int i = 1; i <= len; ++i){
                printf("%02X ", data[i-1]);
                if (i % 64 == 0)
                        printf("\n");
        }
        printf("\n");
#endif
}