/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Shortened most frequently using types.
 */
#pragma once

#include <stdint.h>
#include <netinet/in.h>

typedef uint8_t         u8;
typedef uint16_t        u16;
typedef uint32_t        u32;
typedef uint64_t        u64;

typedef int8_t          i8;
typedef int16_t         i16;
typedef int32_t         i32;
typedef int64_t         i64;

typedef struct sockaddr         saddr;
typedef struct sockaddr_in      saddr_in;
typedef struct sockaddr_in6     saddr_in6;
typedef socklen_t               saddr_l;

