/**
 * Color your output! :)
 */

#pragma once

#define FBLACK      "\033[30m"
#define FRED        "\033[91m"
#define FGREEN      "\033[32m"
#define F_LGREEN    "\033[92m"
#define FYELLOW     "\033[93m"
#define FBLUE       "\033[34m"
#define FPURPLE     "\033[35m"
#define D_FGREEN    "\033[6m"
#define FWHITE      "\033[7m"
#define FCYAN       "\x1b[96m"
#define NONE        "\033[0m"
