/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Set of useful functions.
 */

#pragma once

#include <stdarg.h>
#include <stdint.h>
#include "types.h"

/** Validate int string. */
int validate_int(const char *str);

/** Validate unsigned string. */
uint64_t validate_uint(const char *str);

/** Get host name string and port number string from cmd line data. */
int get_host_data(char *data, char *hbuff, char *pbuff);

/** Get monotonic time with nanosecond resolution. */
u64 get_nanosec();

/** Get epoch time in miliseconds. */
u64 get_epoch_usec();

/** Dump datagram */
void dump_data(const char *name, u8 *data, size_t len);