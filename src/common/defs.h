/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Common definitions & structures
 */

#pragma once

/*------   GAME DEFS     -----*/
/*--- Turn directions defs ---*/
#define TURN_DIR_L              -1
#define TURN_DIR_F              0
#define TURN_DIR_R              1

/*---      Game events     ---*/
#define EVT_NEWGAME             0
#define EVT_PIXEL               1
#define EVT_PLELIM              2
#define EVT_GAMEOVER            3

/*---     Game states      ---*/
#define GSTATE_PREPARE          0
#define GSTATE_RUNNING          1

#define ARENA_FREE              -1
/*------    CONF DEFS    -----*/
/*---    Config limits     ---*/
#define PLAYER_CHAR_MIN         33
#define PLAYER_CHAR_MAX         126
#define PLAYER_NAME_SIZE        64
#define HOST_NAME_SIZE          128
#define PORT_NUM_MAX            65535
#define MAX_WIDTH               1920
#define MAX_HEIGHT              1080
#define MAX_TURN_SPEED          60
#define MAX_GAME_SPEED          250

#define MAX_CLIENTS             42
#define MAX_TIMEOUT             2000000000LU

/*---   Default config     ---*/
#define DEFAULT_WIDTH           800
#define DEFAULT_HEIGHT          600
#define DEFAULT_TURN_SPEED      6
#define DEFAULT_GAME_SPEED      50

#define DEFAULT_SERVER_PORT     12345
#define DEFAULT_GUI_PORT        12346
#define DEFAULT_GUI_HOST        "localhost"

#define CLIENT_REFRESH_RATE     50

/*---     RNG config      ---*/
#define RNG_MOD                 4294967291U
#define RNG_MUL                 279470273U

/*------   APP DEFS     -----*/
/*---    Int logic map    ---*/
#define FALSE                   0
#define TRUE                    1

/*--- Network defs ---*/
#define ADDR_TYPE_IP4           0
#define ADDR_TYPE_IP6           1

#define MAX_DGRAM_SIZE          512
#define MAX_CLIENT_DGRAM_SIZE   77
#define MIN_CLIENT_DGRAM_SIZE   13

#define CLIENT_FREE_SLOT        -1

/*--- Clock defs ---*/
#define NANO_IN_SEC             1000000000lu
#define MICRO_IN_SEC            1000000lu

/*--- Application msg types ---*/
#define MSG_TYPE_PARAMS         "PARAMS"
#define MSG_TYPE_INIT           "INIT"
#define MSG_TYPE_FREE           "FREE"
#define MSG_TYPE_RECV           "RECV"
#define MSG_TYPE_SEND           "SEND"
#define MSG_TYPE_QUIT           "QUIT"
#define MSG_TYPE_GAME           "GAME"
#define MSG_TYPE_DISCONNECT     "DISCONNECT"
