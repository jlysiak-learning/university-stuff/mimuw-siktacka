/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Nanosecond monotonic clock.
 */

#include <stdlib.h>
#include "clock.h"
#include "utils.h"
#include "defs.h"

//==================== MODULE STRUCTURES
typedef struct clk_t {
        u64 last;               //!< Time of last update
        u64 now;                //!< Time now
        u64 left;               //!< Time left to next new round
        u64 interval;           //!< Interval length
        u64 start_time;         //!< Server start time
        uint8_t *flag;          //!< Link to flag which is set with given interval
} clk_t;

//==================== INTERFACE
clk_t *clk_create(u64 interval, u8 *flag)
{
        clk_t *clk = calloc(1, sizeof(clk_t));
        clk->now = get_nanosec();
        clk->start_time = clk->now;
        clk->last = clk->now;
        clk->interval = interval;
        clk->left = interval;
        clk->flag = flag;
        return clk;
}

void clk_free(clk_t **clk)
{
        if (!*clk)
                return;
        free(*clk);
        *clk = NULL;
}

u64 clk_update(clk_t *clk)
{
        clk->now = get_nanosec();
        if (clk->now - clk->last > clk->interval) {
                clk->last = clk->now;
                if (clk->flag)
                        *clk->flag = TRUE;
        }
        clk->left = clk->last + clk->interval - clk->now;
        return clk->left;
}

u64 clk_now(clk_t *clk)
{
        return clk->now;
}


u64 clk_uptime(clk_t *clk)
{
        return clk->now - clk->start_time;
}

u64 clk_left(clk_t *clk)
{
        return clk->left;
}