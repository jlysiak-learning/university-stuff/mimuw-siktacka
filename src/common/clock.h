/**
 * Sieci komputerowe
 * Zadanie 2 - siktacka
 * Autor: Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Nanosecond monotonic clock.
 */
#pragma once

#include "types.h"

//==================== MODULE STRUCTURES
/** Monotonic clock structure */
typedef struct clk_t clk_t;

//==================== INTERFACE
/** Creates clock. */
clk_t *clk_create(u64 interval, u8 *flag);

/** Destroys clock. */
void clk_free(clk_t **clk);

/** Updates actual time.
 * Sets flag whenever time elapsed between call and last
 * update will be greater than precisely set interval.
 * At the end, time left to next event is calculated.*/
u64 clk_update(clk_t *clk);

/** Gets time now. */
u64 clk_now(clk_t *clk);

/** Gets uptime. */
u64 clk_uptime(clk_t *clk);

/** Gets time left to end of interval. */
u64 clk_left(clk_t *clk);